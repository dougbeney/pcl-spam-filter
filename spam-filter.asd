(load (merge-pathnames
       "pathnames-library/pathnames.asd" *load-truename*))

(asdf:defsystem :spam-filter
    :depends-on ("pathnames" "cl-ppcre")
    :serial t
    :components ((:file "packages")
		 (:file "spam-filter")))
